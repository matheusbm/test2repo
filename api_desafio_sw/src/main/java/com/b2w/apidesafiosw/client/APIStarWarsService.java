package com.b2w.apidesafiosw.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.client.RestTemplate;

import com.b2w.apidesafiosw.responses.APIStarWarsResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class APIStarWarsService {

	private String APIStarWarsUrl ;
	private RestTemplate rest ;
	protected static final Logger LOGGER = LoggerFactory.getLogger(APIStarWarsService.class);
	
	@Autowired
	public APIStarWarsService(RestTemplate rest, @Value("${starwars.api.url}") String url) {
		this.rest = rest ;
		APIStarWarsUrl = url ;
	}
	
	private String getUrl(String planeta) {
		return APIStarWarsUrl + "?search=" + planeta;
	}
	
	public Integer retornaNumeroAparicoes(String planeta) {
		try {
			String url = getUrl(planeta) ;
			APIStarWarsResponse result = rest.getForObject(url, APIStarWarsResponse.class);
			return result.getResults().stream().findFirst().get().getFilms().size();
		} catch(Exception e) {
			LOGGER.info("Ocorreu um erro ao pesquisar as aparições em filmes: " + e.getMessage());
			return 0;
		}
	}
}
