package com.b2w.apidesafiosw.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.b2w.apidesafiosw.models.Planet;

public interface PlanetRepository extends MongoRepository<Planet, String> {

	public Planet findOneById(String id);
}
