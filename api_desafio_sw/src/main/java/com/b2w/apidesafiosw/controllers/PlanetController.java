package com.b2w.apidesafiosw.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.b2w.apidesafiosw.models.Planet;
import com.b2w.apidesafiosw.responses.PlanetResponse;
import com.b2w.apidesafiosw.services.PlanetService;

@RestController
@CrossOrigin(origins  = "http://localhost:8080")
@RequestMapping( path = "/planets" )
public class PlanetController {
	
	@Autowired
	private PlanetService planetService;
	
	@GetMapping
	public ResponseEntity< PlanetResponse<List<Planet>>> listarTodos() {
		return ResponseEntity.ok( new PlanetResponse<List<Planet>>( planetService.listarTodos() ) ) ;
	}
	
	@GetMapping( path = "/{id}")
	public ResponseEntity< PlanetResponse<Planet>> listarPorId(@PathVariable( name = "id" ) String id) {		
		return ResponseEntity.ok( new PlanetResponse<Planet>( planetService.listarPorId(id) ) );
	}
	
	@PostMapping
	public ResponseEntity<PlanetResponse<Planet>> cadastrar(@Valid @RequestBody Planet planeta, BindingResult result){
		if (result.hasErrors()) {
			List<String> erros = new ArrayList<String>();
			result.getAllErrors().forEach(erro -> erros.add(erro.getDefaultMessage()));
			return ResponseEntity.badRequest().body( new PlanetResponse<Planet>(erros)) ;
		} 

		return ResponseEntity.ok( new PlanetResponse<Planet>(planetService.cadastrar(planeta) ));
		
	}
	
	@PutMapping( path = "/{id}")
	public ResponseEntity<PlanetResponse<Planet>> atualizar(@PathVariable( name = "id" ) String id, @Valid @RequestBody Planet planeta, BindingResult result){
		if (result.hasErrors()) {
			List<String> erros = new ArrayList<String>();
			result.getAllErrors().forEach(erro -> erros.add(erro.getDefaultMessage()));
			return ResponseEntity.badRequest().body( new PlanetResponse<Planet>(erros)) ;
		}
		
		planeta.setId(id);
		return ResponseEntity.ok( new PlanetResponse<Planet>( planetService.atualizar(planeta) ) );
		
	}
	@DeleteMapping( path = "/{id}" )
	public ResponseEntity<PlanetResponse<Integer>> remover( @PathVariable( name = "id" ) String id ){
		planetService.remover(id);
		return ResponseEntity.ok( new PlanetResponse<Integer>(1));
	}
}
