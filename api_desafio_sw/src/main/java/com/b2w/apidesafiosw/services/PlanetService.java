package com.b2w.apidesafiosw.services;

import java.util.List;

import com.b2w.apidesafiosw.models.Planet;
import com.b2w.apidesafiosw.responses.PlanetResponse;

public interface PlanetService {
       
	List<Planet> listarTodos();
	
	Planet listarPorId( String id ) ;
	
	Planet cadastrar( Planet planeta ) ;

	Planet atualizar( Planet planeta ) ;
	
	void remover( String id	 ) ;
}
