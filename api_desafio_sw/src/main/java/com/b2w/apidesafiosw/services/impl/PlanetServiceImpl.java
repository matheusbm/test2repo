package com.b2w.apidesafiosw.services.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.b2w.apidesafiosw.client.APIStarWarsService;
import com.b2w.apidesafiosw.models.Planet;
import com.b2w.apidesafiosw.repositories.PlanetRepository;
import com.b2w.apidesafiosw.responses.PlanetResponse;
import com.b2w.apidesafiosw.services.PlanetService;

@Service	
public class PlanetServiceImpl implements PlanetService {

	@Autowired
	private PlanetRepository planetRepository ;
	private APIStarWarsService apisw ; 
	
	@Override
	public List<Planet> listarTodos() {
		List<Planet> planets = planetRepository.findAll();
		return retornaListPlanetResponse(planets) ;
	}

	@Override
	public Planet listarPorId(String id) {
		return planetRepository.findOneById(id) ;
	}
	
	@Override
	public Planet cadastrar(Planet planeta) {
		return planetRepository.save(planeta);
	}

	@Override
	public Planet atualizar(Planet planeta) {
		return planetRepository.save(planeta);
	}

	@Override
	public void remover(String id) {
		planetRepository.deleteById(id);

	}
	
	private PlanetResponse<Planet> geraPlanetResponse( Planet planet, Integer numAparicoes ) {
		return new PlanetResponse<Planet>( planet, numAparicoes );
	}
	
	private List<PlanetResponse<Planet>> retornaListPlanetResponse( List<Planet> planets){
		List<PlanetResponse<Planet>> planetsResponse = new ArrayList<PlanetResponse<Planet>>();
		for (Planet planet:planets) {
			Integer numeroAparicoes = apisw.retornaNumeroAparicoes(planet.getNome());
			planetsResponse.add(geraPlanetResponse(planet, numeroAparicoes));
		}
		return planetsResponse ;
	}

}
